/* Mashduino workshop */
// Playing with leds

#define L1 13
#define L2 12
#define L3 11
#define L4 10
#define L5 9
#define L6 8
#define L7 7


void setup () {
  // Setup all pins as output
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (L5, OUTPUT);
  pinMode (L6, OUTPUT);
  pinMode (L7, OUTPUT);
} 

void loop () {
  int delay_value = 100;    // Value used in the delays
  
  // Set them all ON (HIGH)
  digitalWrite (L1, HIGH);  
  delay (delay_value);
  digitalWrite (L2, HIGH);
  delay (delay_value);
  digitalWrite (L3, HIGH);
  delay (delay_value);
  digitalWrite (L4, HIGH);
  delay (delay_value);
  digitalWrite (L5, HIGH);
  delay (delay_value);
  digitalWrite (L6, HIGH);
  delay (delay_value);
  digitalWrite (L7, HIGH);
  delay (delay_value);
  
  // Set them all OFF (LOW)
  digitalWrite (L1, LOW);
  delay (delay_value);  
  digitalWrite (L2, LOW);
  delay (delay_value);
  digitalWrite (L3, LOW);
  delay (delay_value);
  digitalWrite (L4, LOW);
  delay (delay_value);
  digitalWrite (L5, LOW);
  delay (delay_value);
  digitalWrite (L6, LOW);
  delay (delay_value);
  digitalWrite (L7, LOW);
  delay (delay_value);

    // Set them all ON (HIGH)
  digitalWrite (L7, HIGH);  
  delay (delay_value);
  digitalWrite (L6, HIGH);
  delay (delay_value);
  digitalWrite (L5, HIGH);
  delay (delay_value);
  digitalWrite (L4, HIGH);
  delay (delay_value);
  digitalWrite (L3, HIGH);
  delay (delay_value);
  digitalWrite (L2, HIGH);
  delay (delay_value);
  digitalWrite (L1, HIGH);
  delay (delay_value);
  
  // Set them all OFF (LOW)
  digitalWrite (L7, LOW);
  delay (delay_value);  
  digitalWrite (L6, LOW);
  delay (delay_value);
  digitalWrite (L5, LOW);
  delay (delay_value);
  digitalWrite (L4, LOW);
  delay (delay_value);
  digitalWrite (L3, LOW);
  delay (delay_value);
  digitalWrite (L2, LOW);
  delay (delay_value);
  digitalWrite (L1, LOW);
  delay (delay_value);
}


/* Second way of doing the same thing*/
/*

#define L1 13
#define L2 12
#define L3 11
#define L4 10
#define L5 9
#define L6 8
#define L7 7


void setup () {
  // Setup all pins as output
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (L5, OUTPUT);
  pinMode (L6, OUTPUT);
  pinMode (L7, OUTPUT);
} 

void loop () {
  int delay_value = 100;    // Value used in the delays
  int LED_PIN_NUMBER = 0;   // Value used for temporal pin number
  
  // Set them all ON (HIGH)
  for (int a=13; a>=7; a--) {
    LED_PIN_NUMBER = a;
    digitalWrite (LED_PIN_NUMBER, HIGH);  
    delay (delay_value); 
  }
  
  // Set them all OFF (LOW)
  for (int a=13; a>=7; a--) {
    LED_PIN_NUMBER = a;
    digitalWrite (LED_PIN_NUMBER, LOW);  
    delay (delay_value); 
  }
  
  // Set them all ON (HIGH)
  for (int a=7; a<=13; a++) {
    LED_PIN_NUMBER = a;
    digitalWrite (LED_PIN_NUMBER, HIGH);  
    delay (delay_value); 
  }
  
  // Set them all OFF (LOW)
  for (int a=7; a<=13; a++) {
    LED_PIN_NUMBER = a;
    digitalWrite (LED_PIN_NUMBER, LOW);  
    delay (delay_value); 
  }
 
}

*/








