/* Mashduino workshop*/
// Playing with leds

#define L1 13
#define L2 12
#define L3 11
#define L4 10
#define L5 9
#define L6 8
#define L7 7


void setup () {
  // Setup all pins as output
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (L5, OUTPUT);
  pinMode (L6, OUTPUT);
  pinMode (L7, OUTPUT);
} 

void loop () {
  // Set them all ON (HIGH)
  digitalWrite (L1, HIGH);  
  digitalWrite (L2, HIGH);
  digitalWrite (L3, HIGH);
  digitalWrite (L4, HIGH);
  digitalWrite (L5, HIGH);
  digitalWrite (L6, HIGH);
  digitalWrite (L7, HIGH);
  delay (1000);
  
  // Set them all OFF (HIGH)
  digitalWrite (L1, HIGH);  
  digitalWrite (L2, HIGH);
  digitalWrite (L3, HIGH);
  digitalWrite (L4, HIGH);
  digitalWrite (L5, HIGH);
  digitalWrite (L6, HIGH);
  digitalWrite (L7, HIGH);
  delay (1000);
}


