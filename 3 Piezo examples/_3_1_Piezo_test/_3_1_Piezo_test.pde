/* Mashduino workshop */
// Playing with piezo

// define piezo
#define piezo 6


/*

Note frequencies and period

Do = 396Hz = 2.525ms
Re = 417 = 2.398ms
Mi = 528 = 1.894ms
Fa = 639 = 1.565ms
Sol = 741 = 1.35ms
La = 852 = 1.174ms


2.525ms = 2525us

*/

void setup () {
  // Setup Piezo as output
  pinMode (piezo, OUTPUT);
} 

void loop () {
  // generating a do
  digitalWrite (piezo, HIGH);
  delayMicroseconds (1262);   // 1262 is the result of dividing the period of  the note DO (2525) by 2. (wich give us half the period, so time ON and Time OFF)
  digitalWrite (piezo, LOW);
  delayMicroseconds (1262);
}





