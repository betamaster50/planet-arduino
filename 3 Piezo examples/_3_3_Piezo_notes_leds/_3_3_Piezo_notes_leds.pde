/* Mashduino workshop */
// Playing with piezo

// define piezo
#define piezo 6

// define Led pins
#define L1 13
#define L2 12
#define L3 11
#define L4 10
#define L5 9
#define L6 8
#define L7 7

/*

Note frequencies and period

Do = 396Hz = 2.525ms
Re = 417 = 2.398ms
Mi = 528 = 1.894ms
Fa = 639 = 1.565ms
Sol = 741 = 1.35ms
La = 852 = 1.174ms


2.525ms = 2525us

*/

void setup () {
  // Setup Piezo as output
  pinMode (piezo, OUTPUT);
  
  // Setup all LED pins as output
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (L5, OUTPUT);
  pinMode (L6, OUTPUT);
  pinMode (L7, OUTPUT);
} 

void loop () {
  
  set_leds_on (1);
  for (int a = 0; a < 1000; a++) {
    note_period (2525);      // do
  }
  
  set_leds_on (2);
  for (int a = 0; a < 1000; a++) {
    note_period (2398);      // Re
  }
  
  set_leds_on (3);
  for (int a = 0; a < 1000; a++) {
    note_period (1894);      // mi
  }
  
  set_leds_on (4);
  for (int a = 0; a < 1000; a++) {
    note_period (1565);      // fa
  }
  
  set_leds_on (5);
  for (int a = 0; a < 1000; a++) {
    note_period (1350);      // sol
  }
  
  set_leds_on (6);
  for (int a = 0; a < 1000; a++) {
    note_period (1174);      // la
  }

}


void note_period (int period) {   // period needs to be in micro seconds
  
  // generating a note
  int halfPeriod = period/2;
  digitalWrite (piezo, HIGH);
  delayMicroseconds (halfPeriod);   // halfPeriod give us half the period, so time ON and Time OFF)
  digitalWrite (piezo, LOW);
  delayMicroseconds (halfPeriod);  
}






