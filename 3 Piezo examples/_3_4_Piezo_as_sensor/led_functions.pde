
  

void set_leds_on (int number_of_leds) {

  digitalWrite (L1, LOW);  
  digitalWrite (L2, LOW);
  digitalWrite (L3, LOW);
  digitalWrite (L4, LOW);
  digitalWrite (L5, LOW);
  digitalWrite (L6, LOW);
  digitalWrite (L7, LOW);
    
  if (1 <= number_of_leds) {
    digitalWrite (L1, HIGH);  
  }
  
  if (2 <= number_of_leds) {
    digitalWrite (L2, HIGH);  
  }
  
  if (3 <= number_of_leds) {
    digitalWrite (L3, HIGH);  
  }
  
  if (4 <= number_of_leds) {
    digitalWrite (L4, HIGH);  
  }
  
  if (5 <= number_of_leds) {
    digitalWrite (L5, HIGH);  
  }
  
  if (6 <= number_of_leds) {
    digitalWrite (L6, HIGH);  
  }
  
  if (7 <= number_of_leds) {
    digitalWrite (L7, HIGH);  
  }  
}
