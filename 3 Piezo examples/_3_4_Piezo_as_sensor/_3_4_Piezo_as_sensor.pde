/* Mashduino workshop */
// Playing with piezo
// The resistor in prarallel needs to be in the range of megaoms

// define piezo
#define piezo 2

// define Led pins
#define L1 13
#define L2 12
#define L3 11
#define L4 10
#define L5 9
#define L6 8
#define L7 7

void setup () {
  // Setup Piezo as output
  pinMode (piezo, INPUT);
  
  // Setup all LED pins as output
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (L5, OUTPUT);
  pinMode (L6, OUTPUT);
  pinMode (L7, OUTPUT);
} 

void loop () {
  
  int value = analogRead (piezo);
  int new_value = map (value, 0, 1023, 0, 7); 
  
  set_leds_on (new_value);
  

}






