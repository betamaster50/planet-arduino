/* Mashduino workshop */
// Playing with potenciometer

// define potenciometer
#define P0 0

// define Led pins
#define L1 13
#define L2 12
#define L3 11
#define L4 10
#define L5 9
#define L6 8
#define L7 7

void setup () {
  
  // Setup Potenciometer pin as input
  pinMode (P0, INPUT);
  
  Serial.begin (9600);
  Serial.println ("Begin");
  
  // Setup all LED pins as output
  pinMode (L1, OUTPUT);
  pinMode (L2, OUTPUT);
  pinMode (L3, OUTPUT);
  pinMode (L4, OUTPUT);
  pinMode (L5, OUTPUT);
  pinMode (L6, OUTPUT);
  pinMode (L7, OUTPUT);

} 

void loop () {
  int value = analogRead (P0);
  int new_value = map (value, 0, 1023, 0, 7); 
  
  Serial.print (value);
  Serial.print (" - ");
  Serial.println (new_value);
  
  set_leds_on (new_value);    
  
  delay (150);
}


void set_leds_on (int number_of_leds) {

  digitalWrite (L1, LOW);  
  digitalWrite (L2, LOW);
  digitalWrite (L3, LOW);
  digitalWrite (L4, LOW);
  digitalWrite (L5, LOW);
  digitalWrite (L6, LOW);
  digitalWrite (L7, LOW);
    
  if (1 <= number_of_leds) {
    digitalWrite (L1, HIGH);  
  }
  
  if (2 <= number_of_leds) {
    digitalWrite (L2, HIGH);  
  }
  
  if (3 <= number_of_leds) {
    digitalWrite (L3, HIGH);  
  }
  
  if (4 <= number_of_leds) {
    digitalWrite (L4, HIGH);  
  }
  
  if (5 <= number_of_leds) {
    digitalWrite (L5, HIGH);  
  }
  
  if (6 <= number_of_leds) {
    digitalWrite (L6, HIGH);  
  }
  
  if (7 <= number_of_leds) {
    digitalWrite (L7, HIGH);  
  }  
}
